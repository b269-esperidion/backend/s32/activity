//Create a server with routes 

let http = require("http");

const port =4000;

const server = http.createServer((request, response) => {
		/*GET*/
	 if (request.url == "/" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to booking System");
	}
	 else if (request.url == "/profile" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to your profile!");
	} 
	 else if (request.url == "/courses" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Here's our courses available");
	} 
	 else if 
	(request.url == "/updateCourse" && request.method == "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Update a course to our resources");
	}  
	 /*POST*/
	 else if (request.url == "/addCourse" && request.method == "POST"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Add a course to our resources");
	}  
	/*PUT*/
	 else if (request.url == "/archiveCourses" && request.method == "PUT"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Archive courses to our resources");
	} 
	 

});

server.listen(port);

console.log(`Server now accessible at localhost ${port}`);